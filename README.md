This is bionic from android-10-release tag, with
 - the nb-qemu-guest patch (needed for pthreads to work properly)
 - a patch that allows cross-compiling `libc.so` by just running `make` (only clang is supported, 
both because bionic simply intentionally can't be built with gcc and because cross compilation will
just work as we don't need a sysroot)
 - prebuilt linker64 and libdl.so (for aarch64) since we don't need to patch those and building
them without the AOSP build system would be even more painful than with bionic
