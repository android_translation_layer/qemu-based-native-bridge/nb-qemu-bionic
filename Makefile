CURRDIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
BUILDDIR ?= $(CURRDIR)/builddir

LIBC_SRCS_arm64 = \
libc/arch-arm64/bionic/__bionic_clone.S \
libc/arch-arm64/bionic/_exit_with_stack_teardown.S \
libc/arch-arm64/bionic/setjmp.S \
libc/arch-arm64/bionic/syscall.S \
libc/arch-arm64/bionic/vfork.S \
libc/arch-arm64/generic/bionic/memcmp.S \
libc/arch-arm64/generic/bionic/__memcpy_chk.S \
libc/arch-arm64/generic/bionic/strnlen.S \
libc/arch-arm64/generic/bionic/strlen.S \
libc/arch-arm64/generic/bionic/memchr.S \
libc/arch-arm64/generic/bionic/memset.S \
libc/arch-arm64/generic/bionic/strncmp.S \
libc/arch-arm64/generic/bionic/memcpy.S \
libc/arch-arm64/generic/bionic/wmemmove.S \
libc/arch-arm64/generic/bionic/memmove.S \
libc/arch-arm64/generic/bionic/strcmp.S \
libc/arch-arm64/generic/bionic/stpcpy.S \
libc/arch-arm64/generic/bionic/strcpy.S \
libc/arch-arm64/generic/bionic/memcpy_base.S \
libc/arch-arm64/generic/bionic/strchr.S \
libc/arch-arm64/syscalls/__accept4.S \
libc/arch-arm64/syscalls/acct.S \
libc/arch-arm64/syscalls/adjtimex.S \
libc/arch-arm64/syscalls/bind.S \
libc/arch-arm64/syscalls/__brk.S \
libc/arch-arm64/syscalls/capget.S \
libc/arch-arm64/syscalls/capset.S \
libc/arch-arm64/syscalls/chdir.S \
libc/arch-arm64/syscalls/chroot.S \
libc/arch-arm64/syscalls/clock_adjtime.S \
libc/arch-arm64/syscalls/__clock_getres.S \
libc/arch-arm64/syscalls/__clock_gettime.S \
libc/arch-arm64/syscalls/___clock_nanosleep.S \
libc/arch-arm64/syscalls/clock_settime.S \
libc/arch-arm64/syscalls/___close.S \
libc/arch-arm64/syscalls/__connect.S \
libc/arch-arm64/syscalls/delete_module.S \
libc/arch-arm64/syscalls/dup3.S \
libc/arch-arm64/syscalls/dup.S \
libc/arch-arm64/syscalls/epoll_create1.S \
libc/arch-arm64/syscalls/epoll_ctl.S \
libc/arch-arm64/syscalls/__epoll_pwait.S \
libc/arch-arm64/syscalls/eventfd.S \
libc/arch-arm64/syscalls/execve.S \
libc/arch-arm64/syscalls/__exit.S \
libc/arch-arm64/syscalls/_exit.S \
libc/arch-arm64/syscalls/___faccessat.S \
libc/arch-arm64/syscalls/__fadvise64.S \
libc/arch-arm64/syscalls/fallocate.S \
libc/arch-arm64/syscalls/fchdir.S \
libc/arch-arm64/syscalls/___fchmodat.S \
libc/arch-arm64/syscalls/___fchmod.S \
libc/arch-arm64/syscalls/fchownat.S \
libc/arch-arm64/syscalls/fchown.S \
libc/arch-arm64/syscalls/fcntl.S \
libc/arch-arm64/syscalls/fdatasync.S \
libc/arch-arm64/syscalls/___fgetxattr.S \
libc/arch-arm64/syscalls/___flistxattr.S \
libc/arch-arm64/syscalls/flock.S \
libc/arch-arm64/syscalls/fremovexattr.S \
libc/arch-arm64/syscalls/___fsetxattr.S \
libc/arch-arm64/syscalls/fstat64.S \
libc/arch-arm64/syscalls/fstatat64.S \
libc/arch-arm64/syscalls/__fstatfs.S \
libc/arch-arm64/syscalls/fsync.S \
libc/arch-arm64/syscalls/ftruncate.S \
libc/arch-arm64/syscalls/__getcpu.S \
libc/arch-arm64/syscalls/__getcwd.S \
libc/arch-arm64/syscalls/__getdents64.S \
libc/arch-arm64/syscalls/getegid.S \
libc/arch-arm64/syscalls/geteuid.S \
libc/arch-arm64/syscalls/getgid.S \
libc/arch-arm64/syscalls/getgroups.S \
libc/arch-arm64/syscalls/getitimer.S \
libc/arch-arm64/syscalls/getpeername.S \
libc/arch-arm64/syscalls/getpgid.S \
libc/arch-arm64/syscalls/__getpid.S \
libc/arch-arm64/syscalls/getppid.S \
libc/arch-arm64/syscalls/__getpriority.S \
libc/arch-arm64/syscalls/getrandom.S \
libc/arch-arm64/syscalls/getresgid.S \
libc/arch-arm64/syscalls/getresuid.S \
libc/arch-arm64/syscalls/getrlimit.S \
libc/arch-arm64/syscalls/getrusage.S \
libc/arch-arm64/syscalls/getsid.S \
libc/arch-arm64/syscalls/getsockname.S \
libc/arch-arm64/syscalls/getsockopt.S \
libc/arch-arm64/syscalls/__gettimeofday.S \
libc/arch-arm64/syscalls/getuid.S \
libc/arch-arm64/syscalls/getxattr.S \
libc/arch-arm64/syscalls/init_module.S \
libc/arch-arm64/syscalls/inotify_add_watch.S \
libc/arch-arm64/syscalls/inotify_init1.S \
libc/arch-arm64/syscalls/inotify_rm_watch.S \
libc/arch-arm64/syscalls/__ioctl.S \
libc/arch-arm64/syscalls/kill.S \
libc/arch-arm64/syscalls/klogctl.S \
libc/arch-arm64/syscalls/lgetxattr.S \
libc/arch-arm64/syscalls/linkat.S \
libc/arch-arm64/syscalls/listen.S \
libc/arch-arm64/syscalls/listxattr.S \
libc/arch-arm64/syscalls/llistxattr.S \
libc/arch-arm64/syscalls/lremovexattr.S \
libc/arch-arm64/syscalls/lseek.S \
libc/arch-arm64/syscalls/lsetxattr.S \
libc/arch-arm64/syscalls/madvise.S \
libc/arch-arm64/syscalls/mincore.S \
libc/arch-arm64/syscalls/mkdirat.S \
libc/arch-arm64/syscalls/mknodat.S \
libc/arch-arm64/syscalls/mlockall.S \
libc/arch-arm64/syscalls/mlock.S \
libc/arch-arm64/syscalls/mmap.S \
libc/arch-arm64/syscalls/mount.S \
libc/arch-arm64/syscalls/mprotect.S \
libc/arch-arm64/syscalls/___mremap.S \
libc/arch-arm64/syscalls/msync.S \
libc/arch-arm64/syscalls/munlockall.S \
libc/arch-arm64/syscalls/munlock.S \
libc/arch-arm64/syscalls/munmap.S \
libc/arch-arm64/syscalls/nanosleep.S \
libc/arch-arm64/syscalls/__openat.S \
libc/arch-arm64/syscalls/personality.S \
libc/arch-arm64/syscalls/pipe2.S \
libc/arch-arm64/syscalls/__ppoll.S \
libc/arch-arm64/syscalls/prctl.S \
libc/arch-arm64/syscalls/pread64.S \
libc/arch-arm64/syscalls/preadv.S \
libc/arch-arm64/syscalls/prlimit64.S \
libc/arch-arm64/syscalls/process_vm_readv.S \
libc/arch-arm64/syscalls/process_vm_writev.S \
libc/arch-arm64/syscalls/__pselect6.S \
libc/arch-arm64/syscalls/__ptrace.S \
libc/arch-arm64/syscalls/pwrite64.S \
libc/arch-arm64/syscalls/pwritev.S \
libc/arch-arm64/syscalls/quotactl.S \
libc/arch-arm64/syscalls/readahead.S \
libc/arch-arm64/syscalls/readlinkat.S \
libc/arch-arm64/syscalls/read.S \
libc/arch-arm64/syscalls/readv.S \
libc/arch-arm64/syscalls/__reboot.S \
libc/arch-arm64/syscalls/recvfrom.S \
libc/arch-arm64/syscalls/recvmmsg.S \
libc/arch-arm64/syscalls/recvmsg.S \
libc/arch-arm64/syscalls/removexattr.S \
libc/arch-arm64/syscalls/renameat.S \
libc/arch-arm64/syscalls/__rt_sigaction.S \
libc/arch-arm64/syscalls/__rt_sigpending.S \
libc/arch-arm64/syscalls/__rt_sigprocmask.S \
libc/arch-arm64/syscalls/___rt_sigqueueinfo.S \
libc/arch-arm64/syscalls/__rt_sigsuspend.S \
libc/arch-arm64/syscalls/__rt_sigtimedwait.S \
libc/arch-arm64/syscalls/__sched_getaffinity.S \
libc/arch-arm64/syscalls/sched_getparam.S \
libc/arch-arm64/syscalls/sched_get_priority_max.S \
libc/arch-arm64/syscalls/sched_get_priority_min.S \
libc/arch-arm64/syscalls/sched_getscheduler.S \
libc/arch-arm64/syscalls/sched_rr_get_interval.S \
libc/arch-arm64/syscalls/sched_setaffinity.S \
libc/arch-arm64/syscalls/sched_setparam.S \
libc/arch-arm64/syscalls/sched_setscheduler.S \
libc/arch-arm64/syscalls/sched_yield.S \
libc/arch-arm64/syscalls/sendfile.S \
libc/arch-arm64/syscalls/sendmmsg.S \
libc/arch-arm64/syscalls/sendmsg.S \
libc/arch-arm64/syscalls/sendto.S \
libc/arch-arm64/syscalls/setdomainname.S \
libc/arch-arm64/syscalls/setfsgid.S \
libc/arch-arm64/syscalls/setfsuid.S \
libc/arch-arm64/syscalls/setgid.S \
libc/arch-arm64/syscalls/setgroups.S \
libc/arch-arm64/syscalls/sethostname.S \
libc/arch-arm64/syscalls/setitimer.S \
libc/arch-arm64/syscalls/setns.S \
libc/arch-arm64/syscalls/setpgid.S \
libc/arch-arm64/syscalls/setpriority.S \
libc/arch-arm64/syscalls/setregid.S \
libc/arch-arm64/syscalls/setresgid.S \
libc/arch-arm64/syscalls/setresuid.S \
libc/arch-arm64/syscalls/setreuid.S \
libc/arch-arm64/syscalls/setrlimit.S \
libc/arch-arm64/syscalls/setsid.S \
libc/arch-arm64/syscalls/setsockopt.S \
libc/arch-arm64/syscalls/__set_tid_address.S \
libc/arch-arm64/syscalls/settimeofday.S \
libc/arch-arm64/syscalls/setuid.S \
libc/arch-arm64/syscalls/setxattr.S \
libc/arch-arm64/syscalls/shutdown.S \
libc/arch-arm64/syscalls/sigaltstack.S \
libc/arch-arm64/syscalls/__signalfd4.S \
libc/arch-arm64/syscalls/socketpair.S \
libc/arch-arm64/syscalls/__socket.S \
libc/arch-arm64/syscalls/splice.S \
libc/arch-arm64/syscalls/__statfs.S \
libc/arch-arm64/syscalls/swapoff.S \
libc/arch-arm64/syscalls/swapon.S \
libc/arch-arm64/syscalls/symlinkat.S \
libc/arch-arm64/syscalls/__sync_file_range.S \
libc/arch-arm64/syscalls/syncfs.S \
libc/arch-arm64/syscalls/sync.S \
libc/arch-arm64/syscalls/sysinfo.S \
libc/arch-arm64/syscalls/tee.S \
libc/arch-arm64/syscalls/tgkill.S \
libc/arch-arm64/syscalls/__timer_create.S \
libc/arch-arm64/syscalls/__timer_delete.S \
libc/arch-arm64/syscalls/timerfd_create.S \
libc/arch-arm64/syscalls/timerfd_gettime.S \
libc/arch-arm64/syscalls/timerfd_settime.S \
libc/arch-arm64/syscalls/__timer_getoverrun.S \
libc/arch-arm64/syscalls/__timer_gettime.S \
libc/arch-arm64/syscalls/__timer_settime.S \
libc/arch-arm64/syscalls/times.S \
libc/arch-arm64/syscalls/truncate.S \
libc/arch-arm64/syscalls/umask.S \
libc/arch-arm64/syscalls/umount2.S \
libc/arch-arm64/syscalls/uname.S \
libc/arch-arm64/syscalls/unlinkat.S \
libc/arch-arm64/syscalls/unshare.S \
libc/arch-arm64/syscalls/utimensat.S \
libc/arch-arm64/syscalls/vmsplice.S \
libc/arch-arm64/syscalls/wait4.S \
libc/arch-arm64/syscalls/__waitid.S \
libc/arch-arm64/syscalls/write.S \
libc/arch-arm64/syscalls/writev.S

LIBC_NOSP_SRCS = \
libc/bionic/__libc_init_main_thread.cpp \
libc/bionic/__stack_chk_fail.cpp \
libc/arch-arm64/bionic/__set_tls.c

LIBC_SRCS = \
libc/arch-common/bionic/crtbegin_so.c \
libc/async_safe/async_safe_log.cpp \
libc/bionic/abort.cpp \
libc/bionic/accept4.cpp \
libc/bionic/accept.cpp \
libc/bionic/access.cpp \
libc/bionic/android_set_abort_message.cpp \
libc/bionic/arpa_inet.cpp \
libc/bionic/assert.cpp \
libc/bionic/atof.cpp \
libc/bionic/bionic_allocator.cpp \
libc/bionic/bionic_arc4random.cpp \
libc/bionic/bionic_elf_tls.cpp \
libc/bionic/bionic_futex.cpp \
libc/bionic/__bionic_get_shell_path.cpp \
libc/bionic/bionic_netlink.cpp \
libc/bionic/bionic_systrace.cpp \
libc/bionic/bionic_time_conversions.cpp \
libc/bionic/brk.cpp \
libc/bionic/c16rtomb.cpp \
libc/bionic/c32rtomb.cpp \
libc/bionic/chmod.cpp \
libc/bionic/chown.cpp \
libc/bionic/clearenv.cpp \
libc/bionic/clock.cpp \
libc/bionic/clock_getcpuclockid.cpp \
libc/bionic/clock_nanosleep.cpp \
libc/bionic/clone.cpp \
libc/bionic/__cmsg_nxthdr.cpp \
libc/bionic/connect.cpp \
libc/bionic/ctype.cpp \
libc/bionic/__cxa_guard.cpp \
libc/bionic/__cxa_pure_virtual.cpp \
libc/bionic/__cxa_thread_atexit_impl.cpp \
libc/bionic/dirent.cpp \
libc/bionic/dl_iterate_phdr_static.cpp \
libc/bionic/dup2.cpp \
libc/bionic/environ.cpp \
libc/bionic/__errno.cpp \
libc/bionic/error.cpp \
libc/bionic/ether_aton.c \
libc/bionic/ether_ntoa.c \
libc/bionic/eventfd_read.cpp \
libc/bionic/eventfd_write.cpp \
libc/bionic/exec.cpp \
libc/bionic/faccessat.cpp \
libc/bionic/fchmodat.cpp \
libc/bionic/fchmod.cpp \
libc/bionic/fdsan.cpp \
libc/bionic/ffs.cpp \
libc/bionic/fgetxattr.cpp \
libc/bionic/flistxattr.cpp \
libc/bionic/flockfile.cpp \
libc/bionic/fork.cpp \
libc/bionic/fpclassify.cpp \
libc/bionic/fsetxattr.cpp \
libc/bionic/ftruncate.cpp \
libc/bionic/fts.c \
libc/bionic/ftw.cpp \
libc/bionic/futimens.cpp \
libc/bionic/getauxval.cpp \
libc/bionic/getcwd.cpp \
libc/bionic/get_device_api_level.cpp \
libc/bionic/getdomainname.cpp \
libc/bionic/getentropy.cpp \
libc/bionic/gethostname.cpp \
libc/bionic/getloadavg.cpp \
libc/bionic/getpagesize.cpp \
libc/bionic/getpgrp.cpp \
libc/bionic/getpid.cpp \
libc/bionic/getpriority.cpp \
libc/bionic/gettid.cpp \
libc/bionic/__gnu_basename.cpp \
libc/bionic/grp_pwd.cpp \
libc/bionic/grp_pwd_file.cpp \
libc/bionic/iconv.cpp \
libc/bionic/icu.cpp \
libc/bionic/icu_static.cpp \
libc/bionic/icu_wrappers.cpp \
libc/bionic/ifaddrs.cpp \
libc/bionic/initgroups.c \
libc/bionic/inotify_init.cpp \
libc/bionic/ioctl.cpp \
libc/bionic/isatty.c \
libc/bionic/jemalloc_wrapper.cpp \
libc/bionic/killpg.cpp \
libc/bionic/langinfo.cpp \
libc/bionic/lchown.cpp \
libc/bionic/lfs64_support.cpp \
libc/bionic/__libc_current_sigrtmax.cpp \
libc/bionic/__libc_current_sigrtmin.cpp \
libc/bionic/libc_init_common.cpp \
libc/bionic/libc_init_dynamic.cpp \
libc/bionic/libgen.cpp \
libc/bionic/link.cpp \
libc/bionic/locale.cpp \
libc/bionic/lockf.cpp \
libc/bionic/lstat.cpp \
libc/bionic/malloc_common.cpp \
libc/bionic/malloc_common_dynamic.cpp \
libc/bionic/malloc_heapprofd.cpp \
libc/bionic/malloc_limit.cpp \
libc/bionic/mblen.cpp \
libc/bionic/mbrtoc16.cpp \
libc/bionic/mbrtoc32.cpp \
libc/bionic/memmem.cpp \
libc/bionic/mempcpy.cpp \
libc/bionic/mkdir.cpp \
libc/bionic/mkfifo.cpp \
libc/bionic/mknod.cpp \
libc/bionic/mntent.cpp \
libc/bionic/mremap.cpp \
libc/bionic/ndk_cruft.cpp \
libc/bionic/netdb.cpp \
libc/bionic/NetdClient.cpp \
libc/bionic/NetdClientDispatch.cpp \
libc/bionic/net_if.cpp \
libc/bionic/netinet_in.cpp \
libc/bionic/new.cpp \
libc/bionic/nl_types.cpp \
libc/bionic/open.cpp \
libc/bionic/pathconf.cpp \
libc/bionic/pause.cpp \
libc/bionic/pipe.cpp \
libc/bionic/poll.cpp \
libc/bionic/posix_fadvise.cpp \
libc/bionic/posix_fallocate.cpp \
libc/bionic/posix_madvise.cpp \
libc/bionic/posix_timers.cpp \
libc/bionic/pthread_atfork.cpp \
libc/bionic/pthread_attr.cpp \
libc/bionic/pthread_barrier.cpp \
libc/bionic/pthread_cond.cpp \
libc/bionic/pthread_create.cpp \
libc/bionic/pthread_detach.cpp \
libc/bionic/pthread_equal.cpp \
libc/bionic/pthread_exit.cpp \
libc/bionic/pthread_getcpuclockid.cpp \
libc/bionic/pthread_getschedparam.cpp \
libc/bionic/pthread_gettid_np.cpp \
libc/bionic/pthread_internal.cpp \
libc/bionic/pthread_join.cpp \
libc/bionic/pthread_key.cpp \
libc/bionic/pthread_kill.cpp \
libc/bionic/pthread_mutex.cpp \
libc/bionic/pthread_once.cpp \
libc/bionic/pthread_rwlock.cpp \
libc/bionic/pthread_self.cpp \
libc/bionic/pthread_setname_np.cpp \
libc/bionic/pthread_setschedparam.cpp \
libc/bionic/pthread_sigqueue.cpp \
libc/bionic/pthread_spinlock.cpp \
libc/bionic/ptrace.cpp \
libc/bionic/pty.cpp \
libc/bionic/pututline.c \
libc/bionic/raise.cpp \
libc/bionic/rand.cpp \
libc/bionic/readlink.cpp \
libc/bionic/reboot.cpp \
libc/bionic/recv.cpp \
libc/bionic/rename.cpp \
libc/bionic/rmdir.cpp \
libc/bionic/scandir.cpp \
libc/bionic/sched_cpualloc.c \
libc/bionic/sched_cpucount.c \
libc/bionic/sched_getaffinity.cpp \
libc/bionic/sched_getcpu.cpp \
libc/bionic/semaphore.cpp \
libc/bionic/send.cpp \
libc/bionic/setegid.cpp \
libc/bionic/__set_errno.cpp \
libc/bionic/seteuid.cpp \
libc/bionic/setjmp_cookie.cpp \
libc/bionic/setpgrp.cpp \
libc/bionic/sigaction.cpp \
libc/bionic/signal.cpp \
libc/bionic/sigprocmask.cpp \
libc/bionic/socket.cpp \
libc/bionic/spawn.cpp \
libc/bionic/stat.cpp \
libc/bionic/statvfs.cpp \
libc/bionic/stdlib_l.cpp \
libc/bionic/strchrnul.cpp \
libc/bionic/strerror.cpp \
libc/bionic/string_l.cpp \
libc/bionic/strings_l.cpp \
libc/bionic/strrchr.cpp \
libc/bionic/strsignal.cpp \
libc/bionic/strtol.cpp \
libc/bionic/strtold.cpp \
libc/bionic/swab.cpp \
libc/bionic/symlink.cpp \
libc/bionic/sync_file_range.cpp \
libc/bionic/sysconf.cpp \
libc/bionic/sys_epoll.cpp \
libc/bionic/sysinfo.cpp \
libc/bionic/syslog.cpp \
libc/bionic/sys_msg.cpp \
libc/bionic/sys_sem.cpp \
libc/bionic/sys_shm.cpp \
libc/bionic/sys_signalfd.cpp \
libc/bionic/system.cpp \
libc/bionic/sys_time.cpp \
libc/bionic/tdestroy.cpp \
libc/bionic/termios.cpp \
libc/bionic/thread_private.cpp \
libc/bionic/timespec_get.cpp \
libc/bionic/tmpfile.cpp \
libc/bionic/umount.cpp \
libc/bionic/unlink.cpp \
libc/bionic/vdso.cpp \
libc/bionic/wait.cpp \
libc/bionic/wchar.cpp \
libc/bionic/wchar_l.cpp \
libc/bionic/wcstod.cpp \
libc/bionic/wctype.cpp \
libc/bionic/wcwidth.cpp \
libc/bionic/wmempcpy.cpp \
libc/stdio/fmemopen.cpp \
libc/stdio/parsefloat.c \
libc/stdio/refill.c \
libc/stdio/stdio.cpp \
libc/stdio/stdio_ext.cpp \
libc/stdio/vfprintf.cpp \
libc/stdio/vfscanf.cpp \
libc/stdio/vfwprintf.cpp \
libc/stdio/vfwscanf.c \
libc/stdlib/atexit.c \
libc/stdlib/exit.c \
libc/tzcode/asctime.c \
libc/tzcode/bionic.cpp \
libc/tzcode/difftime.c \
libc/tzcode/localtime.c \
libc/tzcode/strftime.c \
libc/tzcode/strptime.c \
libdl/libdl_android.cpp

LIBC_FREEBSD_SRCS = \
libc/upstream-freebsd/lib/libc/gen/glob.c \
libc/upstream-freebsd/lib/libc/gen/ldexp.c \
libc/upstream-freebsd/lib/libc/gen/sleep.c \
libc/upstream-freebsd/lib/libc/gen/usleep.c \
libc/upstream-freebsd/lib/libc/stdlib/getopt_long.c \
libc/upstream-freebsd/lib/libc/stdlib/hcreate.c \
libc/upstream-freebsd/lib/libc/stdlib/hcreate_r.c \
libc/upstream-freebsd/lib/libc/stdlib/hdestroy_r.c \
libc/upstream-freebsd/lib/libc/stdlib/hsearch_r.c \
libc/upstream-freebsd/lib/libc/stdlib/qsort.c \
libc/upstream-freebsd/lib/libc/stdlib/quick_exit.c \
libc/upstream-freebsd/lib/libc/stdlib/realpath.c \
libc/upstream-freebsd/lib/libc/string/wcpcpy.c \
libc/upstream-freebsd/lib/libc/string/wcpncpy.c \
libc/upstream-freebsd/lib/libc/string/wcscasecmp.c \
libc/upstream-freebsd/lib/libc/string/wcscat.c \
libc/upstream-freebsd/lib/libc/string/wcschr.c \
libc/upstream-freebsd/lib/libc/string/wcscmp.c \
libc/upstream-freebsd/lib/libc/string/wcscpy.c \
libc/upstream-freebsd/lib/libc/string/wcscspn.c \
libc/upstream-freebsd/lib/libc/string/wcsdup.c \
libc/upstream-freebsd/lib/libc/string/wcslcat.c \
libc/upstream-freebsd/lib/libc/string/wcslen.c \
libc/upstream-freebsd/lib/libc/string/wcsncasecmp.c \
libc/upstream-freebsd/lib/libc/string/wcsncat.c \
libc/upstream-freebsd/lib/libc/string/wcsncmp.c \
libc/upstream-freebsd/lib/libc/string/wcsncpy.c \
libc/upstream-freebsd/lib/libc/string/wcsnlen.c \
libc/upstream-freebsd/lib/libc/string/wcspbrk.c \
libc/upstream-freebsd/lib/libc/string/wcsrchr.c \
libc/upstream-freebsd/lib/libc/string/wcsspn.c \
libc/upstream-freebsd/lib/libc/string/wcsstr.c \
libc/upstream-freebsd/lib/libc/string/wcstok.c \
libc/upstream-freebsd/lib/libc/string/wmemchr.c \
libc/upstream-freebsd/lib/libc/string/wmemcmp.c \
libc/upstream-freebsd/lib/libc/string/wmemcpy.c \
libc/upstream-freebsd/lib/libc/string/wmemset.c

LIBC_NETBSD_SRCS = \
libc/dns/nameser/ns_name.c \
libc/dns/nameser/ns_netint.c \
libc/dns/nameser/ns_parse.c \
libc/dns/nameser/ns_print.c \
libc/dns/nameser/ns_samedomain.c \
libc/dns/nameser/ns_ttl.c \
libc/dns/net/getaddrinfo.c \
libc/dns/net/gethnamaddr.c \
libc/dns/net/getnameinfo.c \
libc/dns/net/getservent.c \
libc/dns/net/nsdispatch.c \
libc/dns/net/sethostent.c \
libc/dns/resolv/herror.c \
libc/dns/resolv/res_cache.c \
libc/dns/resolv/res_comp.c \
libc/dns/resolv/res_data.c \
libc/dns/resolv/res_debug.c \
libc/dns/resolv/res_init.c \
libc/dns/resolv/res_mkquery.c \
libc/dns/resolv/res_query.c \
libc/dns/resolv/res_send.c \
libc/dns/resolv/res_state.c \
libc/dns/resolv/res_stats.c \
libc/upstream-netbsd/common/lib/libc/hash/sha1/sha1.c \
libc/upstream-netbsd/common/lib/libc/stdlib/random.c \
libc/upstream-netbsd/lib/libc/gen/nice.c \
libc/upstream-netbsd/lib/libc/gen/psignal.c \
libc/upstream-netbsd/lib/libc/gen/utime.c \
libc/upstream-netbsd/lib/libc/gen/utmp.c \
libc/upstream-netbsd/lib/libc/inet/nsap_addr.c \
libc/upstream-netbsd/lib/libc/isc/ev_streams.c \
libc/upstream-netbsd/lib/libc/isc/ev_timers.c \
libc/upstream-netbsd/lib/libc/regex/regcomp.c \
libc/upstream-netbsd/lib/libc/regex/regerror.c \
libc/upstream-netbsd/lib/libc/regex/regexec.c \
libc/upstream-netbsd/lib/libc/regex/regfree.c \
libc/upstream-netbsd/lib/libc/stdlib/bsearch.c \
libc/upstream-netbsd/lib/libc/stdlib/drand48.c \
libc/upstream-netbsd/lib/libc/stdlib/erand48.c \
libc/upstream-netbsd/lib/libc/stdlib/jrand48.c \
libc/upstream-netbsd/lib/libc/stdlib/lcong48.c \
libc/upstream-netbsd/lib/libc/stdlib/lrand48.c \
libc/upstream-netbsd/lib/libc/stdlib/mrand48.c \
libc/upstream-netbsd/lib/libc/stdlib/nrand48.c \
libc/upstream-netbsd/lib/libc/stdlib/_rand48.c \
libc/upstream-netbsd/lib/libc/stdlib/rand_r.c \
libc/upstream-netbsd/lib/libc/stdlib/reallocarr.c \
libc/upstream-netbsd/lib/libc/stdlib/seed48.c \
libc/upstream-netbsd/lib/libc/stdlib/srand48.c

LIBC_OPENBSD_SRCS = \
libc/upstream-openbsd/android/gdtoa_support.cpp \
libc/upstream-openbsd/lib/libc/crypt/arc4random.c \
libc/upstream-openbsd/lib/libc/crypt/arc4random_uniform.c \
libc/upstream-openbsd/lib/libc/gdtoa/dmisc.c \
libc/upstream-openbsd/lib/libc/gdtoa/dtoa.c \
libc/upstream-openbsd/lib/libc/gdtoa/gdtoa.c \
libc/upstream-openbsd/lib/libc/gdtoa/gethex.c \
libc/upstream-openbsd/lib/libc/gdtoa/gmisc.c \
libc/upstream-openbsd/lib/libc/gdtoa/hd_init.c \
libc/upstream-openbsd/lib/libc/gdtoa/hdtoa.c \
libc/upstream-openbsd/lib/libc/gdtoa/hexnan.c \
libc/upstream-openbsd/lib/libc/gdtoa/ldtoa.c \
libc/upstream-openbsd/lib/libc/gdtoa/misc.c \
libc/upstream-openbsd/lib/libc/gdtoa/smisc.c \
libc/upstream-openbsd/lib/libc/gdtoa/strtod.c \
libc/upstream-openbsd/lib/libc/gdtoa/strtodg.c \
libc/upstream-openbsd/lib/libc/gdtoa/strtof.c \
libc/upstream-openbsd/lib/libc/gdtoa/strtord.c \
libc/upstream-openbsd/lib/libc/gdtoa/strtorQ.c \
libc/upstream-openbsd/lib/libc/gdtoa/sum.c \
libc/upstream-openbsd/lib/libc/gdtoa/ulp.c \
libc/upstream-openbsd/lib/libc/gen/alarm.c \
libc/upstream-openbsd/lib/libc/gen/ctype_.c \
libc/upstream-openbsd/lib/libc/gen/daemon.c \
libc/upstream-openbsd/lib/libc/gen/err.c \
libc/upstream-openbsd/lib/libc/gen/errx.c \
libc/upstream-openbsd/lib/libc/gen/fnmatch.c \
libc/upstream-openbsd/lib/libc/gen/ftok.c \
libc/upstream-openbsd/lib/libc/gen/getprogname.c \
libc/upstream-openbsd/lib/libc/gen/isctype.c \
libc/upstream-openbsd/lib/libc/gen/setprogname.c \
libc/upstream-openbsd/lib/libc/gen/tolower_.c \
libc/upstream-openbsd/lib/libc/gen/toupper_.c \
libc/upstream-openbsd/lib/libc/gen/verr.c \
libc/upstream-openbsd/lib/libc/gen/verrx.c \
libc/upstream-openbsd/lib/libc/gen/vwarn.c \
libc/upstream-openbsd/lib/libc/gen/vwarnx.c \
libc/upstream-openbsd/lib/libc/gen/warn.c \
libc/upstream-openbsd/lib/libc/gen/warnx.c \
libc/upstream-openbsd/lib/libc/locale/btowc.c \
libc/upstream-openbsd/lib/libc/locale/mbrlen.c \
libc/upstream-openbsd/lib/libc/locale/mbstowcs.c \
libc/upstream-openbsd/lib/libc/locale/mbtowc.c \
libc/upstream-openbsd/lib/libc/locale/wcscoll.c \
libc/upstream-openbsd/lib/libc/locale/wcstoimax.c \
libc/upstream-openbsd/lib/libc/locale/wcstol.c \
libc/upstream-openbsd/lib/libc/locale/wcstoll.c \
libc/upstream-openbsd/lib/libc/locale/wcstombs.c \
libc/upstream-openbsd/lib/libc/locale/wcstoul.c \
libc/upstream-openbsd/lib/libc/locale/wcstoull.c \
libc/upstream-openbsd/lib/libc/locale/wcstoumax.c \
libc/upstream-openbsd/lib/libc/locale/wcsxfrm.c \
libc/upstream-openbsd/lib/libc/locale/wctob.c \
libc/upstream-openbsd/lib/libc/locale/wctomb.c \
libc/upstream-openbsd/lib/libc/net/base64.c \
libc/upstream-openbsd/lib/libc/net/htonl.c \
libc/upstream-openbsd/lib/libc/net/htons.c \
libc/upstream-openbsd/lib/libc/net/inet_lnaof.c \
libc/upstream-openbsd/lib/libc/net/inet_makeaddr.c \
libc/upstream-openbsd/lib/libc/net/inet_netof.c \
libc/upstream-openbsd/lib/libc/net/inet_ntoa.c \
libc/upstream-openbsd/lib/libc/net/inet_ntop.c \
libc/upstream-openbsd/lib/libc/net/inet_pton.c \
libc/upstream-openbsd/lib/libc/net/ntohl.c \
libc/upstream-openbsd/lib/libc/net/ntohs.c \
libc/upstream-openbsd/lib/libc/net/res_random.c \
libc/upstream-openbsd/lib/libc/stdio/fgetln.c \
libc/upstream-openbsd/lib/libc/stdio/fgetwc.c \
libc/upstream-openbsd/lib/libc/stdio/fgetws.c \
libc/upstream-openbsd/lib/libc/stdio/flags.c \
libc/upstream-openbsd/lib/libc/stdio/fpurge.c \
libc/upstream-openbsd/lib/libc/stdio/fputwc.c \
libc/upstream-openbsd/lib/libc/stdio/fputws.c \
libc/upstream-openbsd/lib/libc/stdio/fvwrite.c \
libc/upstream-openbsd/lib/libc/stdio/fwide.c \
libc/upstream-openbsd/lib/libc/stdio/getdelim.c \
libc/upstream-openbsd/lib/libc/stdio/gets.c \
libc/upstream-openbsd/lib/libc/stdio/makebuf.c \
libc/upstream-openbsd/lib/libc/stdio/mktemp.c \
libc/upstream-openbsd/lib/libc/stdio/open_memstream.c \
libc/upstream-openbsd/lib/libc/stdio/open_wmemstream.c \
libc/upstream-openbsd/lib/libc/stdio/rget.c \
libc/upstream-openbsd/lib/libc/stdio/setvbuf.c \
libc/upstream-openbsd/lib/libc/stdio/tempnam.c \
libc/upstream-openbsd/lib/libc/stdio/tmpnam.c \
libc/upstream-openbsd/lib/libc/stdio/ungetc.c \
libc/upstream-openbsd/lib/libc/stdio/ungetwc.c \
libc/upstream-openbsd/lib/libc/stdio/vasprintf.c \
libc/upstream-openbsd/lib/libc/stdio/vdprintf.c \
libc/upstream-openbsd/lib/libc/stdio/vsscanf.c \
libc/upstream-openbsd/lib/libc/stdio/vswprintf.c \
libc/upstream-openbsd/lib/libc/stdio/vswscanf.c \
libc/upstream-openbsd/lib/libc/stdio/wbuf.c \
libc/upstream-openbsd/lib/libc/stdio/wsetup.c \
libc/upstream-openbsd/lib/libc/stdlib/abs.c \
libc/upstream-openbsd/lib/libc/stdlib/div.c \
libc/upstream-openbsd/lib/libc/stdlib/getenv.c \
libc/upstream-openbsd/lib/libc/stdlib/getsubopt.c \
libc/upstream-openbsd/lib/libc/stdlib/imaxabs.c \
libc/upstream-openbsd/lib/libc/stdlib/imaxdiv.c \
libc/upstream-openbsd/lib/libc/stdlib/insque.c \
libc/upstream-openbsd/lib/libc/stdlib/labs.c \
libc/upstream-openbsd/lib/libc/stdlib/ldiv.c \
libc/upstream-openbsd/lib/libc/stdlib/llabs.c \
libc/upstream-openbsd/lib/libc/stdlib/lldiv.c \
libc/upstream-openbsd/lib/libc/stdlib/lsearch.c \
libc/upstream-openbsd/lib/libc/stdlib/remque.c \
libc/upstream-openbsd/lib/libc/stdlib/setenv.c \
libc/upstream-openbsd/lib/libc/stdlib/tfind.c \
libc/upstream-openbsd/lib/libc/stdlib/tsearch.c \
libc/upstream-openbsd/lib/libc/string/memccpy.c \
libc/upstream-openbsd/lib/libc/string/memrchr.c \
libc/upstream-openbsd/lib/libc/string/stpncpy.c \
libc/upstream-openbsd/lib/libc/string/strcasecmp.c \
libc/upstream-openbsd/lib/libc/string/strcasestr.c \
libc/upstream-openbsd/lib/libc/string/strcat.c \
libc/upstream-openbsd/lib/libc/string/strcoll.c \
libc/upstream-openbsd/lib/libc/string/strcspn.c \
libc/upstream-openbsd/lib/libc/string/strdup.c \
libc/upstream-openbsd/lib/libc/string/strlcat.c \
libc/upstream-openbsd/lib/libc/string/strlcpy.c \
libc/upstream-openbsd/lib/libc/string/strncat.c \
libc/upstream-openbsd/lib/libc/string/strncpy.c \
libc/upstream-openbsd/lib/libc/string/strndup.c \
libc/upstream-openbsd/lib/libc/string/strpbrk.c \
libc/upstream-openbsd/lib/libc/string/strsep.c \
libc/upstream-openbsd/lib/libc/string/strspn.c \
libc/upstream-openbsd/lib/libc/string/strstr.c \
libc/upstream-openbsd/lib/libc/string/strtok.c \
libc/upstream-openbsd/lib/libc/string/strxfrm.c \
libc/upstream-openbsd/lib/libc/string/wcslcpy.c \
libc/upstream-openbsd/lib/libc/string/wcswidth.c \
libc/upstream-openbsd/lib/libc/time/wcsftime.c

LIBC_FORTIFY_SRC= \
libc/bionic/fortify.cpp

# implemented in assembly on arm64

#libc/bionic/strchr.cpp \
#libc/bionic/strnlen.c \

#libc/upstream-freebsd/lib/libc/string/wmemmove.c \

#libc/upstream-openbsd/lib/libc/string/memchr.c \
#libc/upstream-openbsd/lib/libc/string/stpcpy.c \
#libc/upstream-openbsd/lib/libc/string/strcpy.c \
#libc/upstream-openbsd/lib/libc/string/strncmp.c \

JEMALLOC_SRCS = jemalloc/src/arena.c \
jemalloc/src/atomic.c \
jemalloc/src/base.c \
jemalloc/src/bitmap.c \
jemalloc/src/chunk.c \
jemalloc/src/chunk_dss.c \
jemalloc/src/chunk_mmap.c \
jemalloc/src/ckh.c \
jemalloc/src/ctl.c \
jemalloc/src/extent.c \
jemalloc/src/hash.c \
jemalloc/src/huge.c \
jemalloc/src/jemalloc.c \
jemalloc/src/mb.c \
jemalloc/src/mutex.c \
jemalloc/src/nstime.c \
jemalloc/src/pages.c \
jemalloc/src/prng.c \
jemalloc/src/prof.c \
jemalloc/src/quarantine.c \
jemalloc/src/rtree.c \
jemalloc/src/spin.c \
jemalloc/src/stats.c \
jemalloc/src/tcache.c \
jemalloc/src/ticker.c \
jemalloc/src/tsd.c \
jemalloc/src/util.c \
jemalloc/src/witness.c

LIBC_OBJS := $(LIBC_SRCS:%=$(BUILDDIR)/%.o)
LIBC_OBJS_arm64 := $(LIBC_SRCS_arm64:%=$(BUILDDIR)/%.o)
LIBC_NOSP_OBJS := $(LIBC_NOSP_SRCS:%=$(BUILDDIR)/%.o)
LIBC_FORTIFY_OBJ := $(LIBC_FORTIFY_SRC:%=$(BUILDDIR)/%.o)
LIBC_FREEBSD_OBJS := $(LIBC_FREEBSD_SRCS:%=$(BUILDDIR)/%.o)
LIBC_NETBSD_OBJS := $(LIBC_NETBSD_SRCS:%=$(BUILDDIR)/%.o)
LIBC_OPENBSD_OBJS := $(LIBC_OPENBSD_SRCS:%=$(BUILDDIR)/%.o)
JEMALLOC_OBJS := $(JEMALLOC_SRCS:%=$(BUILDDIR)/%.o)

ALL_OBJS = $(LIBC_OBJS) $(LIBC_OBJS_arm64) $(LIBC_NOSP_OBJS) $(LIBC_FORTIFY_OBJ) $(LIBC_FREEBSD_OBJS) $(LIBC_NETBSD_OBJS) $(LIBC_OPENBSD_OBJS) $(JEMALLOC_OBJS)

DEFINES=-D_LIBC=1 \
        -D__ANDROID__=1 \
        -D__BIONIC_LP32_USE_STAT64 \
        -DANDROID_CHANGES \
        -DPOSIX_MISTAKE \
        -DALL_STATE \
        -DSTD_INSPIRED \
        -DTHREAD_SAFE \
        -DTM_GMTOFF=tm_gmtoff \
        -DTZDIR=\"/system/usr/share/zoneinfo\" \
        -DHAVE_POSIX_DECLS=0 \
        -DUSG_COMPAT=1 \
        -DWILDABBR=\"\" \
        -DNO_RUN_TIME_WARNINGS_ABOUT_YEAR_2000_PROBLEMS_THANK_YOU 

ARCH=aarch64
CFLAGS=-target $(ARCH)-pc-linux -g -fPIC -nostdlib -nostdinc -fno-exceptions

INCLUDES=-I$(CURRDIR)/libc/include \
         -I$(CURRDIR)/libc/kernel/android/uapi/ \
         -I$(CURRDIR)/libc/kernel/uapi/ \
         -I$(CURRDIR)/libc/kernel/uapi/asm-arm64/ \
         -I$(CURRDIR)/libc/async_safe/include \
         -I$(CURRDIR)/libc/ \
         -I$(CURRDIR)/libc/private/ \
         -I$(CURRDIR)/libc/bionic \
         -I$(CURRDIR)/libc/stdio/ \
         -I$(CURRDIR)/libc/tzcode/ \
         -I$(CURRDIR)/libc/dns/include/ \
         -I$(CURRDIR)/libc/upstream-freebsd/android/include/ \
         -I$(CURRDIR)/libc/upstream-netbsd/android/include/ \
         -I$(CURRDIR)/libc/upstream-netbsd/lib/libc/include/ \
         -I$(CURRDIR)/libc/upstream-openbsd/android/include/ \
         -I$(CURRDIR)/libc/upstream-openbsd/lib/libc/include/ \
         -I$(CURRDIR)/libc/upstream-openbsd/lib/libc/gdtoa \
         -I$(CURRDIR)/libc/upstream-openbsd/lib/libc/stdio/ \
         -I$(CURRDIR)/libstdc++/include/ \
         -I$(CURRDIR)/jemalloc/include/ \
         -I$(CURRDIR)/_system_core_include/ \
         -I$(shell clang -print-resource-dir)/include/

default: $(BUILDDIR)/libc.so

define rules=
$(filter %.$(1).o,$(LIBC_OBJS)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -include $(CURRDIR)/libc/kernel/uapi/linux/libc-compat.h -o $$@ $$<
$(filter %.$(1).o,$(LIBC_NOSP_OBJS)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -include $(CURRDIR)/libc/kernel/uapi/linux/libc-compat.h -fno-stack-protector -o $$@ $$<
$(filter %.$(1).o,$(LIBC_FORTIFY_OBJ)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -D__BIONIC_DECLARE_FORTIFY_HELPERS -DNO___MEMCPY_CHK -o $$@ $$<
$(filter %.$(1).o,$(LIBC_OBJS_arm64)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -D__ASSEMBLY__ -o $$@ $$<
$(filter %.$(1).o,$(LIBC_FREEBSD_OBJS)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -include $(CURRDIR)/libc/upstream-freebsd/android/include/freebsd-compat.h -o $$@ $$<
$(filter %.$(1).o,$(LIBC_NETBSD_OBJS)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -include $(CURRDIR)/libc/upstream-netbsd/android/include/netbsd-compat.h -o $$@ $$<
$(filter %.$(1).o,$(LIBC_OPENBSD_OBJS)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -include $(CURRDIR)/libc/upstream-openbsd/android/include/openbsd-compat.h -o $$@ $$<
$(filter %.$(1).o,$(JEMALLOC_OBJS)): $(BUILDDIR)/%.$(1).o: %.$(1)
	mkdir -p $$(@D)
	clang -c $(CFLAGS) $(DEFINES) $(INCLUDES) -include $(CURRDIR)/jemalloc/android/include/log.h -o $$@ $$<
endef

EXTS := c cpp S
$(foreach ext, $(EXTS), $(eval $(call rules,$(ext))))

$(BUILDDIR)/libc.so: $(ALL_OBJS)
	clang -target aarch64-pc-linux -fuse-ld=lld -shared -nostdlib -lgcc -L$(CURRDIR)/prebuilts -ldl -l:linker64 -Wl,--version-script,$(CURRDIR)/libc/libc.arm64.map -o $@ -Wl,-soname,libc.so $^

clean:
	rm -rf $(BUILDDIR)

